package mobile_automation_framework.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DataMgr {
    private static String FilePath = "//conf//";
    private static String DataFilePath = "//data//";
    static JSONParser parser = new JSONParser();

    public static String fetchObject(String textvalue) throws Throwable {
	JSONObject jsonObject = getJSONURL("ObjectRepository.json");
	String text = (String) jsonObject.get(textvalue);
	return text;
    }

    /**
     * Fetches the Json array object whose data decsription value matches the
     * identifier
     * 
     * @param textvalue
     * @param Identifier
     * @param Datadesc
     * @param File
     * @return
     * @throws Throwable
     */
    public static JSONObject fetchJsonArrayObject(String textvalue,
	    String Identifier, String Datadesc, String File) throws Throwable {
	JSONObject jsonObject = getJSONURL(File);
	JSONArray user = (JSONArray) jsonObject.get(textvalue);
	Iterator i = user.iterator();
	while (i.hasNext()) {
	    JSONObject innerObj = (JSONObject) i.next();
	    String var = (String) innerObj.get(Datadesc);
	    if (var.equals(Identifier)) {
		return (innerObj);
	    }
	}

	return null;
    }

    public static JSONObject fetchDataJsonArrayObject(String textvalue,
	    String Identifier, String Datadesc, String File) throws Throwable {
	JSONObject jsonObject = getJSONDataURL(File);
	JSONArray user = (JSONArray) jsonObject.get(textvalue);
	Iterator i = user.iterator();
	while (i.hasNext()) {
	    JSONObject innerObj = (JSONObject) i.next();
	    String var = (String) innerObj.get(Datadesc);
	    if (var.equals(Identifier)) {
		return (innerObj);
	    }
	}

	return null;
    }

    /**
     * Returns the Reference of Json Object to the file
     * 
     * @param File
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParseException
     */
    public static JSONObject getJSONURL(String File) throws FileNotFoundException, IOException, ParseException {
	String basePath = new File("").getAbsolutePath();
	Object obj = parser.parse(new FileReader(basePath + FilePath + File));
	JSONObject jsonObject = (JSONObject) obj;
	return jsonObject;
    }

    public static JSONObject getJSONDataURL(String File)
	    throws FileNotFoundException, IOException, ParseException {
	String basePath = new File("").getAbsolutePath();
	Object obj = parser
		.parse(new FileReader(basePath + DataFilePath + File));
	JSONObject jsonObject = (JSONObject) obj;
	return jsonObject;
    }

    @SuppressWarnings("rawtypes")
    public static String getSite(JSONObject JsonFileObject) {
	String url = null;

	url = (String) ((HashMap) JsonFileObject).get(JsonFileObject
		.get("Concept"));

	return url;
    }
}
