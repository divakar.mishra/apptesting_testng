package mobile_automation_framework.utils;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;

public class LocatorConfig {
	
	public static By  changeLangEnglishButton ;
	public static By  heading ;
	public static By  existingUserButton ;
	public static By  existingUserHeading ;
	public static By  userIdTextBox ;
	public static By  passwordTextBox ;
	public static By  loginButton ;
	public static By  myAccountHeading ;
	
	public static By  iosChangeLangButton ;
	public static By  iosHeading ;
	public static By  iosExistingUserButton ;
	public static By  iosExistingUserHeading ;
	public static By  iosUserIdTextBox ;
	public static By  iosPasswordTextBox ;
	public static By  iosLoginButton ;
	public static By  iosMyAccountHeading ;
	
	
		
	public static void getTestData() throws Throwable {
		JSONObject jsonObject = DataMgr.getJSONURL("ObjectRepository.json");
		
		changeLangEnglishButton = By.id((String) jsonObject.get("changeLangEnglishButton"));
		heading = By.id((String) jsonObject.get("heading"));
		existingUserButton = By.id((String) jsonObject.get("existingUserButton"));
		existingUserHeading = By.id((String) jsonObject.get("existingUserHeading"));
		userIdTextBox = By.id((String) jsonObject.get("userIdTextBox"));
		passwordTextBox = By.id((String) jsonObject.get("passwordTextBox"));
		loginButton = By.id((String) jsonObject.get("loginButton"));
		myAccountHeading = By.id((String) jsonObject.get("myAccountHeading"));
		
		
		
		iosChangeLangButton = By.xpath((String) jsonObject.get("iosChangeLangButton"));
		iosHeading = By.id((String) jsonObject.get("iosHeading"));
		iosExistingUserButton = By.xpath((String) jsonObject.get("iosExistingUserButton"));
		iosExistingUserHeading = By.id((String) jsonObject.get("iosExistingUserHeading"));
		iosUserIdTextBox = By.xpath((String) jsonObject.get("iosUserIdTextBox"));
		iosPasswordTextBox = By.xpath((String) jsonObject.get("iosPasswordTextBox"));
		iosLoginButton = By.id((String) jsonObject.get("iosLoginButton"));
		iosMyAccountHeading = By.id((String) jsonObject.get("iosMyAccountHeading"));
		
	}
}
