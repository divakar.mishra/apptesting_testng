package mobile_automation_framework.utils;

import java.io.File;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Selenium_Functions {
	
	public static IOSDriver<MobileElement> Launch_iOSDriver(IOSDriver<MobileElement> driver){
		System.out.println("Launching iOS Driver.......");
		try {
		File app = null;

		File classpathRoot = new File(System.getProperty("user.dir"));
		File appDir = new File(classpathRoot, "/Apps/");
		app = new File(appDir,"ADDC_iOS_Simulator.app");
		
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		
		desiredCapabilities.setCapability("deviceName","iPhone 11");
		desiredCapabilities.setCapability("platformName", "iOS");
		desiredCapabilities.setCapability("platformVersion","13.5");
		desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
		desiredCapabilities.setCapability("newCommandTimeout", "30000");
		desiredCapabilities.setCapability("app", app.getAbsolutePath());
		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new IOSDriver<MobileElement>(url, desiredCapabilities);
		
		
		}catch (Exception e) {
			System.out.println("Exception :"+e);
			return Launch_iOSDriver(driver);
		}
		return driver;	
	}
	
	public static  AndroidDriver<MobileElement> Launch_AndroidDriver( AndroidDriver<MobileElement> driver){
		
			System.out.println("Launching Android Driver......");
		try {
			File app = null;

			File classpathRoot = new File(System.getProperty("user.dir"));
			File appDir = new File(classpathRoot, "/Apps/");
			app = new File(appDir,"ADDC Mobile.apk");
			
			DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
			
			desiredCapabilities.setCapability("deviceName","emulator-5554");
			desiredCapabilities.setCapability("platformName", "android");
			desiredCapabilities.setCapability("platformVersion","10");
			desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
			desiredCapabilities.setCapability("newCommandTimeout", "30000");
			desiredCapabilities.setCapability("app", app.getAbsolutePath());
			
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			driver = new AndroidDriver<MobileElement>(url, desiredCapabilities); 
			
		}catch (Exception e) {
			System.out.println("Exception :"+e);
			return Launch_AndroidDriver(driver);
		}
		return driver;
			
	}
	
	public static boolean WaitTillVisible(WebDriver driver, By locator) {

		for (int i = 0; i < 90; i++) {
			if ((driver.findElement(locator).getSize().height > 0)
					|| (driver.findElement(locator).getSize().width > 0)) {
				return true;
			}
			sleepCode("500");
		}
		throw new IllegalStateException("Element is not visible even after waiting for 45 sec");
	}
	
	public static void sleepCode(String slTime) {
		try {
			long L = Long.parseLong(slTime);
			Thread.sleep(L);
		} catch (Exception e) {
		}
	}
	
	public static boolean isElementPresent(AppiumDriver<MobileElement> driver, By elemLocator) {

		try {
			driver.findElement(elemLocator);
			WaitTillVisible(driver, elemLocator);
			sleepCode("500");
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static void clickElement(AppiumDriver<MobileElement> driver, By elemLocator) {
		if (isElementPresent(driver, elemLocator)) {
			driver.findElement(elemLocator).click();

		} else {
			throw new IllegalStateException("The object " + elemLocator + " is not available on the current page");
		}

	}
	
	public static String getText(AppiumDriver<MobileElement> driver, By elemLocator) {
		if (Selenium_Functions.isElementPresent(driver, elemLocator)) {
			return driver.findElement(elemLocator).getText().toString();

		} else {
			throw new IllegalStateException("The object " + elemLocator + " is not available on the current page");
		}

	}
	
	public static void enterText(AppiumDriver<MobileElement> driver, By elemLocator, String text) {
		if (isElementPresent(driver, elemLocator)) {
			Selenium_Functions.sleepCode("100");
			driver.findElement(elemLocator).clear();
			driver.findElement(elemLocator).sendKeys(text);
		} else {
			throw new IllegalStateException("The object " + elemLocator + " is not available on the current page");
		}
	}
}
