package mobile_automation_framework.pages;

import mobile_automation_framework.utils.EnvConfig;
import mobile_automation_framework.utils.LocatorConfig;
import mobile_automation_framework.utils.Selenium_Functions;

import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.junit.Assert;

@ScenarioScoped
public class LoginPage {

	
	public static boolean isLoginPageDisplayedAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.heading));
				String heading = Selenium_Functions.getText(driver, LocatorConfig.heading);
				Assert.assertEquals("Homepage Heading is not correct", "Welcome to ADDC", heading);
				
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.existingUserHeading));
				String existingUserHeading = Selenium_Functions.getText(driver, LocatorConfig.existingUserHeading);
				Assert.assertEquals("Existing User Heading is not correct","Existing user", existingUserHeading);
				return true;
		} 
		
	public static boolean isLoginPageDisplayedIOS(IOSDriver<MobileElement> driver) throws Throwable {
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.iosHeading));
				String heading = Selenium_Functions.getText(driver, LocatorConfig.iosHeading);
				Assert.assertEquals("Homepage Heading is not correct", "Welcome to ADDC", heading);
			
				Assert.assertTrue("Welcome Heading on Login Page is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.iosExistingUserHeading));
				String existingUserHeading = Selenium_Functions.getText(driver, LocatorConfig.iosExistingUserHeading);
				Assert.assertEquals("Existing User Heading is not correct", "Existing user", existingUserHeading);
				return true;
		}
	
	public static void enterUserIDAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
		 	Selenium_Functions.enterText(driver, LocatorConfig.userIdTextBox, EnvConfig.userId);	
	}
	
	public static void enterUserIDIOS(IOSDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.enterText(driver, LocatorConfig.iosUserIdTextBox, EnvConfig.userId);	
	}
	
	public static void enterPasswordAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.enterText(driver, LocatorConfig.passwordTextBox, EnvConfig.password);
	}
	
	public static void enterPasswordIOS(IOSDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.enterText(driver, LocatorConfig.iosPasswordTextBox, EnvConfig.password);	
	}
	
	public static void clickLoginButtonAndroid(AndroidDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.clickElement(driver, LocatorConfig.loginButton);	
	}
	
	public static void clickLoginButtonIOS(IOSDriver<MobileElement> driver) throws Throwable {
			Selenium_Functions.clickElement(driver, LocatorConfig.iosLoginButton);		
	}
	
}