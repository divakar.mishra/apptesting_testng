package mobile_automation_framework.pages;

import mobile_automation_framework.utils.LocatorConfig;
import mobile_automation_framework.utils.Selenium_Functions;


import cucumber.runtime.java.guice.ScenarioScoped;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.junit.Assert;

@ScenarioScoped
public class MyAccountPage {
	
	public static boolean ismyAccountPageDisplayedAndroid(AndroidDriver<MobileElement>  driver) throws Throwable {
		Assert.assertTrue("Welcome Heading is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.myAccountHeading));
		String heading = Selenium_Functions.getText(driver, LocatorConfig.myAccountHeading);
		Assert.assertEquals("My Account Heading is not correct", "My account", heading);
		return true;
		}
		
	public static boolean ismyAccountPageDisplayedIOS(IOSDriver<MobileElement>  driver) throws Throwable {
		Assert.assertTrue("Welcome Heading is not Displayed", Selenium_Functions.isElementPresent(driver, LocatorConfig.iosMyAccountHeading));
		String heading = Selenium_Functions.getText(driver, LocatorConfig.iosMyAccountHeading);
		Assert.assertEquals("My Account Heading is not correct","My account", heading);
		return true;
	}
	
}