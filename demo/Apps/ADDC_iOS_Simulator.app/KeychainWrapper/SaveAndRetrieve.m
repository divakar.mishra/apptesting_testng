//
//  SaveAndRetrieve.m
//  StoringValuesInKeychain
//
//  Created by AthithChandra on 31/03/16.
//  Copyright © 2016 Wipro Technology. All rights reserved.
//

#import "SaveAndRetrieve.h"

#define SERVICE_NAME @"ADDC"//Any name can be given
//#define GROUP_NAME   @"com.ADDC.addcApp"   //@"PM7352S8QE.com.apps.shared" - Value must be like this - NOT REQUIRED

@implementation SaveAndRetrieve

#pragma mark - INITIALISATION
- (id)init
{
    _myKeychain = [[Keychain alloc] initWithService:SERVICE_NAME withGroup:nil];
    
    return self;
}

#pragma mark - SAVING VALUES TO KEYCHAIN
-(void) saveValuesintoKeychain:(NSString *)strKey AndValue:(NSString *)strValue
{
    NSData * value = [strValue dataUsingEncoding:NSUTF8StringEncoding];
    
    if([_myKeychain insert:strKey :value])
    {
        [self showMessage:@"Successfully added data"];
    }
    else
        [self showMessage:@"Failed to  add data"];
   
}

#pragma mark - UPDATE VALUES TO KEYCHAIN
-(void) updateItemToKeyChain:(NSString *)strKey AndValue:(NSString *)strValue
{
    NSData * data =[_myKeychain find:strKey];
    if(data)
    {
        NSData * value = [strValue dataUsingEncoding:NSUTF8StringEncoding];
        
        if([_myKeychain update:strKey :value])
        {
            [self showMessage:@"Successfully updated data"];
        }
        else
            [self showMessage:@"Failed to update data"];
        
    }
    else
    {
        [self showMessage:@"Key Not found"];
    }
 
}

#pragma mark - FIND VALUES IN KEYCHAIN
-(NSString *) findItemInKeyChain:(NSString *)strKey
{
    
    NSData * data =[_myKeychain find:strKey];
    
    if(data)
    {
        [self showMessage:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    }
    else
    {
        [self showMessage:@"Failed to get Data"];
    }
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
 
}

#pragma mark - DELETE VALUES FROM KEYCHAIN
-(void) deleteItemFromKeyChain:(NSString *)strKey
{
    
    if([_myKeychain remove:strKey])
    {
        [self showMessage:@"Successfully removed data"];
    }
    else
    {
        [self showMessage:@"Unable to remove data"];
    }
 
}


-(BOOL) isJailbroken //need to check whether the device is jailbreak or normal one
{
#if TARGET_IPHONE_SIMULATOR
    return NO;
#else
    FILE *f = fopen("/bin/bash", "r");
    if (errno == ENOENT)
    {
        // device is NOT jailbroken
        fclose(f);
       // DLog(@"no");
        return NO;
    }
    else {
        // device IS jailbroken
        fclose(f);
        //DLog(@"yes");
        return YES;
        
    }
#endif
}
#pragma mark - ALERT MESSAGES
-(void) showMessage:(NSString*)text
{
  //  DLog(@"MESSAGE : %@",text);
}

@end
