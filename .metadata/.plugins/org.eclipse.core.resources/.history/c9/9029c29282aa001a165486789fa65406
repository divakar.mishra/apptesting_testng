<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>testngapptesting</groupId>
	<artifactId>demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>testngapptesting_demo</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
		<cucumberTestNG>4.2.0</cucumberTestNG>
		<cucumberversion>4.1.0</cucumberversion>
	</properties>

	<dependencies>
		<!-- https://mvnrepository.com/artifact/junit/junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.testng/testng -->
		<dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.5.2</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-jvm</artifactId>
			<version>3.0.0</version>
			<type>pom</type>
		</dependency>
		<!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-jvm-deps -->
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-jvm-deps</artifactId>
			<version>1.0.6</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>net.masterthought</groupId>
			<artifactId>cucumber-reporting</artifactId>
			<version>2.0.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-java -->
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-java</artifactId>
			<version>2.0.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-junit -->
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-junit</artifactId>
			<version>2.0.0</version>
			<scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/com.google.inject/guice -->
		<dependency>
			<groupId>com.google.inject</groupId>
			<artifactId>guice</artifactId>
			<version>4.2.3</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/io.cucumber/cucumber-guice -->
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-guice</artifactId>
			<version>4.2.2</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/io.appium/java-client -->
		<dependency>
			<groupId>io.appium</groupId>
			<artifactId>java-client</artifactId>
			<version>7.3.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple -->
		<dependency>
			<groupId>com.googlecode.json-simple</groupId>
			<artifactId>json-simple</artifactId>
			<version>1.1</version>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.17</version>
		</dependency>
		<dependency>
			<groupId>net.masterthought</groupId>
			<artifactId>cucumber-reporting</artifactId>
			<version>2.0.0</version>
		</dependency>

	</dependencies>

	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<targetPath>${project.build.directory}</targetPath>
				<includes>
					<include>log4j.properties</include>
				</includes>
			</resource>
		</resources>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>2.6</version>
					<executions>
						<execution>
							<goals>
								<goal>test-jar</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-dependency-plugin</artifactId>
					<version>2.10</version>
					<executions>
						<execution>
							<id>copy-dependencies</id>
							<phase>package</phase>
							<goals>
								<goal>copy-dependencies</goal>
							</goals>
							<configuration>
								<outputDirectory>${project.build.directory}/dependency-jars/</outputDirectory>
							</configuration>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>2.14.1</version>
					<configuration>
						<!-- Suite testng xml file to consider for test execution -->
						<suiteXmlFiles>
							<suiteXmlFile>testng.xml</suiteXmlFile>
						</suiteXmlFiles>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-failsafe-plugin</artifactId>
					<version>2.16</version>
					<configuration>
						<forkCount>${failsafe.fork.count}</forkCount>
						<reuseForks>false</reuseForks>
						<includes>
							<include>**/*testng.xml</include>
						</includes>
						<runOrder>alphabetical</runOrder>
						<argLine>-Duser.language=en</argLine>
						<argLine>-Dfile.encoding=UTF-8</argLine>
					</configuration>
				</plugin>
				<plugin>
					<groupId>net.masterthought</groupId>
					<artifactId>maven-cucumber-reporting</artifactId>
					<version>3.20.0</version>
					<executions>
						<execution>
							<id>cucumber-report</id>
							<phase>test</phase>
							<goals>
								<goal>generate</goal>
							</goals>
							<configuration>
								<projectName>Automation</projectName>
								<outputDirectory>${project.build.directory}/</outputDirectory>
								<inputDirectory>${project.build.directory}/</inputDirectory>
								<jsonFiles>
									<param>**/*.json</param>
								</jsonFiles>
								<classificationDirectory>${project.build.directory}/</classificationDirectory>
								<classificationFiles>
									<param>sample.properties</param>
									<param>other.properties</param>
								</classificationFiles>
								<cucumberOutput>${project.build.directory}/cucumber-reports</cucumberOutput>
								<skippedFails>true</skippedFails>
								<undefinedFails>true</undefinedFails>
								<enableFlashCharts>true</enableFlashCharts>
								<parallelTesting>false</parallelTesting>
								<checkBuildResult>true</checkBuildResult>
							</configuration>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
</project>